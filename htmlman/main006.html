<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"
            "http://www.w3.org/TR/REC-html40/loose.dtd">
<HTML>
<HEAD>

<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="hevea 1.06-7 of 2001-11-14">
<TITLE>
 Hints on writing IDL files
</TITLE>
</HEAD>
<BODY >
<A HREF="main005.html"><IMG SRC ="previous_motif.gif" ALT="Previous"></A>
<A HREF="index.html"><IMG SRC ="contents_motif.gif" ALT="Contents"></A>
<A HREF="main007.html"><IMG SRC ="next_motif.gif" ALT="Next"></A>
<HR>

<H2><A NAME="htoc29">6</A>&nbsp;&nbsp;Hints on writing IDL files</H2>
<A NAME="toc24"></A>
<H3><A NAME="htoc30">6.1</A>&nbsp;&nbsp;Writing an IDL file for a C library</H3>
When writing an IDL file for a C library that doesn't have an IDL interface
already, the include files for that library are a good starting point:
just copy the relevant type and functin declarations to the IDL file,
then annotate them with IDL attributes to describe more precisely
their actual behavior. The documentation of the library must be read
carefully to determine the mode of function parameters (<TT>in</TT>, <TT>out</TT>,
<TT>inout</TT>), the actual sizes of arrays, etc.<BR>
<BR>
The type definitions in the IDL file need not correspond exactly with
those in the include files. Often, a cleaner Caml interface can be
obtained by omitting irrelevant struct fields, or changing their types.
For instance, the Unix library functions for reading library entries
may use the following structure:
<PRE>
        struct dirent {
            long int d_ino;
            __off_t d_off;
            unsigned short int d_reclen;
            unsigned char d_type;
            char d_name[256];
        };
</PRE>Of those fields, only <TT>d_name</TT> and <TT>d_ino</TT> are of interest to the
user; the other fields are internal information for the library
functions, are not specified in the POSIX specs, and therefore must
not be used. Thus, in the IDL file, you should declare:
<PRE>
        struct dirent {
            long int d_ino;
            char d_name[256];
        };
</PRE>Thus, the Caml code will have 
<TT>type struct_dirent = {d_ino: int; d_name: string}</TT>
as desired. However, the generated stub code, being
compiled against the ``true'' definition of <TT>struct dirent</TT>, will find
those two fields at the correct offsets in the actual struct.<BR>
<BR>
Special attention must be paid to integer fields or variables. 
By default, integer IDL types are mapped to the Caml type <TT>int</TT>,
which is convenient to use in Caml code, but loses one bit
when converting from a C <TT>long</TT> integer, and may lose one bit (on
32-bit platforms) when converting from a C <TT>int</TT> integer. When the
range of values represented by the C integer is small enough, this
loss is acceptable. Otherwise, you should use the attributes
<TT>nativeint</TT>, <TT>int32</TT> or <TT>int64</TT> so that integer IDL types are mapped
to one of the Caml boxed integer types. (We recommend that you use
<TT>int32</TT> or <TT>int64</TT> for integers that are specified as being exactly 32
bit wide or 64 bit wide, and <TT>nativeint</TT> for unspecified <TT>int</TT> or
<TT>long</TT> integers.)<BR>
<BR>
Yet another possibility is to declare certain integer fields or variables
as <TT>double</TT> in the IDL file, so that they are represented by <TT>float</TT>
in Caml, and all 32 bits of the integer are preserved in Caml. For
instance, the Unix function to get the current type is declared as
<PRE>
        time_t time(time_t * t);
</PRE>where <TT>time_t</TT> is usually defined as <TT>long</TT>. We can nonetheless
pretend (in the IDL file) that <TT>time</TT> returns a double:
<PRE>
        double time() quote(" _res = time(NULL); ");
</PRE>This way, <TT>time</TT> will have the Caml type <TT>unit -&gt; float</TT>.
Again, the stub code ``knows'' that <TT>time</TT> actually returns an integer,
and therefore will insert the right integer-float coercions.<BR>
<BR>
<A NAME="toc25"></A>
<H3><A NAME="htoc31">6.2</A>&nbsp;&nbsp;Sharing IDL files between MIDL and CamlIDL</H3>
The Microsoft software development kit provides a number of IDL files
describing various libraries and components. In its current state,
<TT>camlidl</TT> cannot exploit those files directly: they use many
(often poorly documented) Microsoft IDL features that are not
implemented yet in <TT>camlidl</TT>; symmetrically, <TT>camlidl</TT> introduces
several new annotations that are not recognized by Microsoft's <TT>midl</TT>
compiler. So, significant editing work on the IDL files is required.<BR>
<BR>
The C preprocessor can be used to alleviate the <TT>camlidl</TT>-<TT>midl</TT>
incompatibilities: <TT>camlidl</TT> defines the preprocessor symbol <TT>CAMLIDL</TT>
when preprocessing its input files, while <TT>midl</TT> does not. Hence,
one can bracket incompatible definitions in 
<TT>#ifdef CAMLIDL ... #else ... #endif</TT>. Along these lines, a C
preprocessor header file, <TT>camlidlcompat.h</TT>, is provided: it uses
<TT>#define</TT> to remove <TT>camlidl</TT>-specific attributes when compiling with
<TT>midl</TT>, and to remove <TT>midl</TT>-specific attributes when compiling with
<TT>camlidl</TT>. Thus, an IDL file compatible with both <TT>midl</TT> and
<TT>camlidl</TT> would look like this:
<PRE>
        #include &lt;camlidlcompat.h&gt;

        #ifndef CAMLIDL
        import "unknwn.idl";    // imports specific to MIDL
        import "oaidl.idl";
        #endif
        import "mymodule.idl";  // imports common to MIDL and CamlIDL

        typedef [abstract,marshal_as(int)] void * ptr;

        ...

        #ifndef CAMLIDL
        [...] library MyTypeLib {
          importlib("stdole32.tlb");
          [...] coclass MyComponent { [default] interface IX; }
        }
        #endif
</PRE>Notice that since <TT>camlidl</TT> doesn't handle type libraries, the type
library part of an <TT>midl</TT> file must be enclosed in <TT>#ifndef CAMLIDL</TT>.<BR>
<BR>
<A NAME="toc26"></A>
<H3><A NAME="htoc32">6.3</A>&nbsp;&nbsp;Dispatch interfaces and type libraries</H3> <A NAME="s-dispatch"></A>
A dispatch interface, in COM lingo, is an interface that supports
dynamic, interpreted dispatch of method interfaces. This form of
interpreted dispatch is used by Visual Basic and other scripting
languages to perform calls to methods of COM components.<BR>
<BR>
CamlIDL provides minimal support for dispatch interfaces. To equip a
Caml component with a dispatch interface (thus making it callable from
Visual Basic), you need to do the following:
<OL type=1><LI>
Use <TT>IDispatch</TT> instead of <TT>IUnknown</TT> as the super-interface of
the component's interfaces.
<LI>Write a type library for your component and compile it using
<TT>midl</TT>. A type library is a run-time representation of the interfaces
supported by an object. The <TT>midl</TT> compiler can generate a type
library from the IDL description of the component, enriched with some
special-purpose declarations (the <TT>library</TT> and <TT>coclass</TT>
statements). Refer to the documentation of <TT>midl</TT> for more
information.
<LI>Pass the type library files (<TT>.tlb</TT> files) generated by <TT>midl</TT>
as extra arguments to <TT>camlidldll</TT> when generating the DLL for your
Caml component.
</OL>
<HR>
<A HREF="main005.html"><IMG SRC ="previous_motif.gif" ALT="Previous"></A>
<A HREF="index.html"><IMG SRC ="contents_motif.gif" ALT="Contents"></A>
<A HREF="main007.html"><IMG SRC ="next_motif.gif" ALT="Next"></A>
</BODY>
</HTML>
