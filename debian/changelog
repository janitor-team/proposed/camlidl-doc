camlidl-doc (1.04-5) unstable; urgency=low

  * Team upload
  * Switch packaging to git
  * Switch source format to 3.0 (quilt)
  * Switch copyright format to 1.0
  * Bump debhelper compat level to 13 (Closes: #965450)
  * Bump Standards-Version to 4.5.0
  * Add Rules-Requires-Root: no

 -- Stéphane Glondu <glondu@debian.org>  Thu, 23 Jul 2020 13:21:38 +0200

camlidl-doc (1.04-4) unstable; urgency=low

  [ Stefano Zacchiroli ]
  * remove myself from Uploaders
  * debian/control
    - bump Standards-Versions to 3.9.2
    - add misc ${misc:Depends}

  [ Ralf Treinen ]
  * Changed doc-base section to Programming/OCaml

 -- Stefano Zacchiroli <zack@debian.org>  Sat, 08 Oct 2011 16:55:23 +0200

camlidl-doc (1.04-3) unstable; urgency=low

  * update standards-version, no changed needed
  * setting me as an uploader, d-o-m as the maintainer
  * convert the package to a non-native Debian package

 -- Stefano Zacchiroli <zack@debian.org>  Fri, 28 Dec 2007 19:34:29 +0100

camlidl-doc (1.04-2) unstable; urgency=low

  * debian/rules
    - moved debhelper compatibility level to debian/compat and bumped it to 5
    - switched to cdbs, as a side effect this also adds the binary-arch target
      required per policy 4.9 (closes: #395582)
    - avoid compressing the PDF version of the documentation
  * debian/control
    - bumped standards-version to 3.7.2
    - added build deps on cdbs
    - changed debhelper and cdbs to be build-dep (rather than build-dep-indep)
  * debian/copyright
    - updated FSF address
  * debian/*
    - brought packaging up to date wrt current standards (e.g. use .dirs files
      for creating directories, .install files for installing stuff in the
      right place, ...)
  * debian/camlidl-doc.doc-base
    - fixed path reference to the PDF documentation, previous path was wrong

 -- Stefano Zacchiroli <zack@debian.org>  Sun,  5 Nov 2006 16:11:59 +0100

camlidl-doc (1.04-1) unstable; urgency=low

  * First release (documentation for camlidl)
  * Included doc-base description (Closes: Bug#174532)

 -- Stefano Zacchiroli <zack@debian.org>  Fri,  7 Mar 2003 15:52:43 +0100
